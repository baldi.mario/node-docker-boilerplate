.DEFAULT_GOAL := help
SHELL := /bin/bash
TARGET ?= develop

help: ## This help
	@echo -e "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"
build: ## Build docker containers
	@docker-compose -f .docker/docker-compose.${TARGET}.yml build
up: ## Setup docker containers for dev enviroment
	@docker-compose -f .docker/docker-compose.${TARGET}.yml up -d
down: ## Tier down docker containers
	@docker-compose -f .docker/docker-compose.${TARGET}.yml down
ps: ## List docker containers
	@docker-compose -f .docker/docker-compose.${TARGET}.yml ps
install: ## Install npm packages
	@docker-compose -f .docker/docker-compose.${TARGET}.yml exec node npm install
test: ## Run tests
	@docker-compose -f .docker/docker-compose.${TARGET}.yml exec node npm run test
test-watch: ## Run tests in watch mode
	@docker-compose -f .docker/docker-compose.${TARGET}.yml exec node npm run test-watch
compile: ## Compile typescript
	@docker-compose -f .docker/docker-compose.${TARGET}.yml exec node npm run compile
compile-watch: ## Compile typescript in watch mode
	@docker-compose -f .docker/docker-compose.${TARGET}.yml exec node npm run compile-watch
shell: ## Shell in docker container
	@docker-compose -f .docker/docker-compose.${TARGET}.yml exec node sh
run: ## Start index.js in running container
	@docker-compose -f .docker/docker-compose.${TARGET}.yml exec node npm start
start: ## Sart index.js in new container
	@docker-compose -f .docker/docker-compose.${TARGET}.yml run node npm start
wait: ## Start container and keep it alive
	@docker-compose -f .docker/docker-compose.${TARGET}.yml run node tail -f /dev/null