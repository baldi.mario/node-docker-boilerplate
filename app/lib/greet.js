"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Greet = void 0;
/**
 * Simple class to build greet message
 *
 * @export
 * @class Greet
 */
class Greet {
    /**
     * This method builds the green message for someone
     *
     * @param {string} name
     * @return {*}  {string}
     * @memberof Greet
     */
    greet(who) {
        if (who)
            return `Hello, ${who}!`;
        else
            throw new Error('Cannot greet :(');
    }
}
exports.Greet = Greet;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JlZXQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvbGliL2dyZWV0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBOzs7OztHQUtHO0FBQ0gsTUFBYSxLQUFLO0lBQ2Q7Ozs7OztPQU1HO0lBQ0gsS0FBSyxDQUFDLEdBQVc7UUFDYixJQUFJLEdBQUc7WUFDSCxPQUFPLFVBQVUsR0FBRyxHQUFHLENBQUM7O1lBRXhCLE1BQU0sSUFBSSxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUMzQyxDQUFDO0NBQ0o7QUFkRCxzQkFjQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogU2ltcGxlIGNsYXNzIHRvIGJ1aWxkIGdyZWV0IG1lc3NhZ2VcbiAqXG4gKiBAZXhwb3J0XG4gKiBAY2xhc3MgR3JlZXRcbiAqL1xuZXhwb3J0IGNsYXNzIEdyZWV0IHtcbiAgICAvKipcbiAgICAgKiBUaGlzIG1ldGhvZCBidWlsZHMgdGhlIGdyZWVuIG1lc3NhZ2UgZm9yIHNvbWVvbmUgXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZVxuICAgICAqIEByZXR1cm4geyp9ICB7c3RyaW5nfVxuICAgICAqIEBtZW1iZXJvZiBHcmVldFxuICAgICAqL1xuICAgIGdyZWV0KHdobzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICAgICAgaWYgKHdobylcbiAgICAgICAgICAgIHJldHVybiBgSGVsbG8sICR7d2hvfSFgO1xuICAgICAgICBlbHNlXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Nhbm5vdCBncmVldCA6KCcpO1xuICAgIH1cbn0iXX0=