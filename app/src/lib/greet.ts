/**
 * Simple class to build greet message
 *
 * @export
 * @class Greet
 */
export class Greet {
    /**
     * This method builds the green message for someone 
     *
     * @param {string} name
     * @return {*}  {string}
     * @memberof Greet
     */
    greet(who: string): string {
        if (who)
            return `Hello, ${who}!`;
        else
            throw new Error('Cannot greet :(');
    }
}