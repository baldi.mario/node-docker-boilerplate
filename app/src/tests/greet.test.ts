import { Greet } from "../lib/greet";

test('dummy test', () => {
    expect(true).toBe(true);
});

test('Greet someone test', () => {
    let greet: Greet = new Greet();
    expect(greet.greet('World')).toBe('Hello, World!');
});


test('Greet fails', () => {
    let greet: Greet = new Greet();

    expect(() => {
        greet.greet(null);
    }).toThrow('Cannot greet :(');

    expect(() => {
        greet.greet(undefined);
    }).toThrow('Cannot greet :(');
});
