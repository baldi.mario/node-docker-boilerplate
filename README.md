# Node Docker Boilerplate

## Make

```sh
$ make
help           This help
build          Build docker containers
up             Setup docker containers for dev enviroment
down           Tier down docker containers
ps             List docker containers
install        Install npm packages
test           Run tests
test-watch     Run tests in watch mode
compile        Compile typescript
compile-watch  Compile typescript in watch mode
shell          Shell in docker container
run            Start index.js in running container
start          Sart index.js in new container
wait           Start container and keep it alive
```

## Development lifecycle

make build
make install
make wait
make compile-watch
make test-watch
make run

## Production lifecycle

make build
make install
make start
